section .text

; Константы
%define SYS_EXIT 60
%define SYS_WRITE 1
%define STDIN 0
%define STDOUT 1
%define STDERR 2
%define NULL 0


; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax

    .loop:
        cmp byte [rdi+rax], NULL
        je .exit
        inc rax
        jmp .loop
    .exit
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
	mov rdx, rax
	mov rax, SYS_WRITE
	mov rdi, STDOUT
    syscall
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rsi, rsp
	pop rdi
	mov rax, SYS_WRITE
	mov rdi, STDOUT
	mov rdx, 1
	syscall
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, 10 ; основания СС
    push rbp
    mov rbp, rsp
    push NULL
    mov rsi, rsp
    sub rsp, 20 ; макс. Кол-во цифр в числе

    mov rax, rdi

    .loop:
        xor rdx, rdx
        div r8
        add dl, '0'
        dec rsi
        mov [rsi], dl
        test rax, rax
        jnz .loop

    mov rdi, rsi

    call print_string
    
    mov rsp, rbp
    pop rbp
    ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test   rdi, rdi
    jge   print_uint
    push  rdi
    mov   rdi, `-`
    call  print_char
    pop   rdi
    neg   rdi
    jmp print_uint

  

string_equals:
    xor rax, rax

    .loop:
        mov al, [rdi]
        mov dl, [rsi]
        cmp al, dl
        jne .not_equal
        test al, al
        je .exit
        inc rdi
        inc rsi
        jmp .loop

    .not_equal:
        xor rax, rax
        ret

    .exit:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    cmp rax, -1
    pop rax
    jne .exit ; если нет ошибки то выходим
    xor rax,rax
    .exit:
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор


read_word:
    push r13
    push r12
    push r14
    xor rax, rax
    xor r13, r13
    mov r14, rdi 
    mov r12, rsi

    .whitespace:
        ; Пропуск пробельные символы в начале слова
        call read_char

        test  rax, rax
        je    .end_word
        cmp   al, `\t`
        je    .whitespace  
        cmp   al, `\n`
        je    .whitespace
        cmp   al, ` `
        je    .whitespace

    .next:
        cmp r13, r12
        jae .return_zero

        mov [r14+r13], al
        inc r13

        call read_char

        cmp   al, `\t`
        je    .end_word  
        cmp   al, `\n`
        je    .end_word
        cmp   al, ` `
        je    .end_word

        test  rax, rax
        jne    .next

    .end_word:
        mov byte [r14+r13], NULL

        mov rax, r14   
        mov rdx, r13 
        pop r14
        pop r12
        pop r13
        ret

    .return_zero:
        xor rax, rax
        pop r14
        pop r12
        pop r13
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor   rcx, rcx
    xor   rax, rax
    xor   rdx, rdx

    .loop:
        cmp byte[rdi+rdx], `0`
        jl .exit
        cmp byte[rdi+rdx], `9`
        jg .exit


        mov cl, [rdi+rdx] 
        sub cl, `0`
        imul rax, `\n` 
    
        add rax, rcx
        inc rdx 
        jmp .loop
    .exit:
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp   byte[rdi], `-`
    jne   parse_uint
    inc   rdi
    call  parse_uint
    neg   rax
    inc   rdx
    cmp rdx, 1
    je .err
    ret
    .err:
        xor rdx, rdx
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx

    .copy_loop:
        mov al, [rdi + rcx]

        test al, al
        je .end_copy
        cmp rcx, rdx
        jae .buffer_overflow
        mov [rsi + rcx], al
        inc rcx
        inc rax
        jmp .copy_loop

    .buffer_overflow:
        xor rax, rax

    .end_copy:
        mov byte [rsi + rcx], NULL
        ret
